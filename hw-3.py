#__add,mul,sub,truediv
class Calculator:
    def __init__(self, number):
        self.number = number

    def __add__(self, other):
        return self.number + other.number
    def __mul__(self, other):
        return self.number - other.number
    def __sub__(self, other):
        return self.number * other.number
    def __truediv__(self, other):
        return self.number / other.number

number1 = Calculator(10)
number2 = Calculator(5)
print('Addition:', number1 + number2)
print('Substraction:', number1 - number2)
print('Multiplication:', number1 * number2)
print('Divison:', number1 / number2)